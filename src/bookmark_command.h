/* Bluefish HTML Editor
 * bookmark_commands.h - bookmarks from external commands
 *
 * Copyright (C) 2024 Olivier Sessink
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BOOKMARK_COMMANDS_H__
#define __BOOKMARK_COMMANDS_H__

void bookmark_command_from_arr(Tbfwin *bfwin, const gchar **arr);

#endif							/* __BOOKMARK_COMMANDS_H__ */
