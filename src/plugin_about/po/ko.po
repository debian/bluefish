# Korean language file for Bluefish.
# Copyright (C) 2000 Free Software Foundation, Inc.
# Jin-Hwan Jeong <yongdoria@gmail.com>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: Bluefish 2.2.2\n"
"Report-Msgid-Bugs-To: olivier@bluefish.openoffice.nl\n"
"POT-Creation-Date: 2024-09-21 21:24+0200\n"
"PO-Revision-Date: 2009-05-26 08:48+0900\n"
"Last-Translator: Taehun Kang 강태헌 <u4800330@anu.edu.au>\n"
"Language-Team: \n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/plugin_about/about.c:131
#, fuzzy
msgid ""
"This version of Bluefish was built with:\n"
"\n"
msgstr "현재 버젼은 아래와 같이 빌드되었습니다:\n"

#: src/plugin_about/about.c:224
msgid ""
"\n"
"Developers of previous releases:"
msgstr ""
"\n"
"이전 버젼 개발자들:"

#: src/plugin_about/about.c:254
#, fuzzy
msgid ""
"\n"
"If you know of anyone missing from this list,\n"
"please let us know at:"
msgstr ""
"\n"
"이 리스트에 빠진 분들이 계시면,\n"
"<bluefish@bluefish.openoffice.nl> 여길 통해 연락주세요."

#: src/plugin_about/about.c:255
msgid "bluefish@bluefish.openoffice.nl <bluefish@bluefish.openoffice.nl>"
msgstr ""

#: src/plugin_about/about.c:256
#, fuzzy
msgid ""
"\n"
"Thanks to all who helped make this software available.\n"
msgstr ""
"\n"
"이 소프트웨어를 만드는데 도움을 주신 모든 분께 감사드립니다."

#: src/plugin_about/about.c:292
msgid ""
"An open-source editor for experienced web designers and programmers, "
"supporting many programming and markup languages, but focusing on creating "
"dynamic and interactive websites."
msgstr ""
"숙련된 디자이너와 프로그래머들을 위한 오픈소스 에디터로, 많은 프로그래밍 언어"
"와 마크업 언어들을 지원할 뿐만 아니라, 다이나믹/인터랙티브 웹페이지들을 만드"
"는 데 주력합니다."

#. Translators: This is a special message that shouldn't be translated
#. * literally. It is used in the about box to give credits to
#. * the translators.
#. * Thus, you should translate it to your name and email address and
#. * the name and email address of all translator who have contributed
#. * to this translation (in general: this special locale). Please
#. * use a separate line for every translator ending with a newlines (\n).
#.
#: src/plugin_about/about.c:302
msgid "translator-credits"
msgstr ""
"Jin-Hwan Jeong <yongdoria@gmail.com>\n"
"Taehun Kang <u4800330@anu.edu.au>"

#: src/plugin_about/about.c:400
msgid "_Help"
msgstr "도움말(_H)"

#: src/plugin_about/about.c:401
msgid "Bluefish _Homepage"
msgstr "Bluefish 홈페이지(_H)"

#: src/plugin_about/about.c:401
msgid "Bluefish homepage"
msgstr "Bluefish 홈페이지"

#: src/plugin_about/about.c:403
msgid "Report a _Bug"
msgstr "버그 리포트하기(_B)"

#: src/plugin_about/about.c:403
msgid "Report a bug"
msgstr "버그 리포트하기"

#: src/plugin_about/about.c:404
msgid "_About"
msgstr "_About"

#: src/plugin_about/about.c:404
msgid "About Bluefish"
msgstr "About Bluefish"

#: src/plugin_about/about.c:406
#, fuzzy
msgid "Build _Info"
msgstr "빌드 정보(_B)..."

#: src/plugin_about/about.c:406
msgid "Build info"
msgstr "빌드 정보"

#: src/plugin_about/about.c:465
msgid "About Dialog"
msgstr ""

#~ msgid ""
#~ "\n"
#~ "Package maintainers:"
#~ msgstr ""
#~ "\n"
#~ "패키지 메인테이너:"

#~ msgid "/Help/_About..."
#~ msgstr "/도움말/Bluefish는(_A)..."
